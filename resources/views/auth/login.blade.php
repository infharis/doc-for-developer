@extends('layouts.index')
@section('title','Login')
@section('content')
<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
   <div class="panel panel-info">
      <div class="panel-heading">
         <div class="panel-title">Sign In</div>
         <!-- <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div> -->
      </div>
      <div style="padding-top:30px" class="panel-body">
            @include('notif')
         <form id="loginform" class="form-horizontal" role="form" action="{!! URL::to('login') !!}" method="post">
             {!! csrf_field() !!}
            <div style="margin-bottom: 25px" class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
               <input id="login-username" type="email" class="form-control" name="email" placeholder="Email">                                        
            </div>
            <div style="margin-bottom: 10px" class="input-group">
               <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
               <input id="password" type="password" class="form-control" name="password" placeholder="password">
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="remember"> Remember me</label>
            </div>
            <div style="margin-top:10px" class="form-group">
               <!-- Button -->
               <div class="col-sm-12 controls">
                  <input type="submit" class="btn btn-success" value="Sign In">
                  <!-- <a id="btn-fblogin" href="#" class="btn btn-primary">Login with Facebook</a> -->
               </div>
            </div>
            <div class="form-group">
               <div class="col-md-12 control">
                  <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                     Don't have account?
                     <a href="{!! URL::to('daftar') !!}">
                     Sign up Here
                     </a>
                  </div>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
@stop
