@extends('admin.layouts.index')
@section('content')
 <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">

                        
                         <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Data Desa</h3>
                                        <a href="{!! URL::to('create/pasien') !!}" class="btn btn-primary pull-right">Add</a>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <table  class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_4">
                                                    <thead>
                                                        <tr>
                                                            <th>Nama</th>
                                                            <th>Keliling M2</th>   
                                                        </tr>
                                                    </thead>

                                             
                                                    <tbody>
                                                   
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div> <!-- End Row -->

                    </div> <!-- container -->
                               
                </div> <!-- content -->

                <footer class="footer text-right">
                    2015 © Moltran.
                </footer>

            </div>


@stop
@section('js')
<script type="text/javascript">
     $(document).ready(function(){
        $(document).on("click",'.delete',function(){
            if(confirm('apakah anda yakin ingin menghapus biodata pasien ini?'))
            {
                return true;
            }
            return false;
        });
     });
</script>
@stop