<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
       

        <link rel="shortcut icon" href="images/favicon_1.ico">

        <title>Admin - Dashboard Puskesmas Lamteuba</title>

        <!-- Base Css Files -->
        <link href="{!! URL::to('resources/assets/admin/css/bootstrap.min.css')!!}" rel="stylesheet" />

        <!-- Font Icons -->
        <link href="{!! URL::to('resources/assets/admin/assets/font-awesome/css/font-awesome.min.css')!!}" rel="stylesheet" />
        <link href="{!! URL::to('resources/assets/admin/assets/ionicon/css/ionicons.min.css')!!}" rel="stylesheet" />
        <link href="{!! URL::to('resources/assets/admin/css/material-design-iconic-font.min.css')!!}" rel="stylesheet">

        <!-- animate css -->
        <link href="{!! URL::to('resources/assets/admin/css/animate.css')!!}" rel="stylesheet" />

        <!-- Waves-effect -->
        <link href="{!! URL::to('resources/assets/admin/css/waves-effect.css')!!}" rel="stylesheet">

        <!-- sweet alerts -->
        <link href="{!! URL::to('resources/assets/admin/assets/sweet-alert/sweet-alert.min.css')!!}" rel="stylesheet">

        <!-- Custom Files -->
        <link href="{!! URL::to('resources/assets/admin/css/helper.css')!!}" rel="stylesheet" type="text/css" />
        <link href="{!! URL::to('resources/assets/admin/css/style.css')!!}" rel="stylesheet" type="text/css" />

        <link href="{!! URL::to('resources/assets/admin/datatables/datatables.min.css')!!}" rel="stylesheet" type="text/css" />
<link href="{!! URL::to('resources/assets/admin/datatables/datatables.bootstrap.css')!!}" rel="stylesheet" type="text/css" />

        
        
    </head>



    <body class="fixed-left">
        
        <!-- Begin page -->
        <div id="wrapper">
        
            @include('admin.layouts.topbar')
            <!-- Top Bar End -->
           @include('admin.layouts.sidebar')
            @yield('content')
            <!-- Right Sidebar -->
           
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->


    
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="{!! URL::to('resources/assets/admin/js/jquery.min.js')!!}"></script>
        <script src="{!! URL::to('resources/assets/admin/js/bootstrap.min.js')!!}"></script>
    
        <script src="{!! URL::to('resources/assets/admin/assets/chat/moment-2.2.1.js')!!}"></script>
        <script src="{!! URL::to('resources/assets/admin/assets/jquery-sparkline/jquery.sparkline.min.js')!!}"></script>
        <script src="{!! URL::to('resources/assets/admin/assets/jquery-detectmobile/detect.js')!!}"></script>
        <script src="{!! URL::to('resources/assets/admin/assets/fastclick/fastclick.js')!!}"></script>
     
        <script src="{!! URL::to('resources/assets/admin/assets/jquery-blockui/jquery.blockUI.js')!!}"></script>

      
        <!-- Counter-up -->
        <script src="{!! URL::to('resources/assets/admin/assets/counterup/waypoints.min.js')!!}" type="text/javascript"></script>
        <script src="{!! URL::to('resources/assets/admin/assets/counterup/jquery.counterup.min.js')!!}" type="text/javascript"></script>
        
        <!-- CUSTOM JS -->
        <script src="{!! URL::to('resources/assets/admin/js/jquery.app.js')!!}"></script>

        <!-- Dashboard -->
        <script src="{!! URL::to('resources/assets/admin/js/jquery.dashboard.js')!!}"></script>
        <script src="{!! URL::to('resources/assets/admin/datatable.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('resources/assets/admin/datatables/datatables.min.js')!!}" type="text/javascript"></script>
    <script src="{!! URL::to('resources/assets/admin/datatables/plugins/bootstrap/datatables.bootstrap.js')!!}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{!! URL::to('resources/assets/admin/table-datatables-responsive.min.js')!!}" type="text/javascript"></script>
    @yield('js')
    </body>
</html>