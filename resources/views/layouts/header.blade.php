<nav class="navbar navbar-default navbar-static-top">
   <div class="container">
      <div class="navbar-header">
         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         </button>
         <a class="navbar-brand" href="#">Documentation For Developer</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
         <ul class="nav navbar-nav navbar-right">
            <li><a href="{!! URL::to('doc') !!}"><span class="glyphicon glyphicon-book"></span> Dokumentasi</a></li>
            @if(!Auth::user())
            <li><a href="{!! URL::to('daftar') !!}"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            <li><a href="{!! URL::to('login') !!}"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            @else 
            <ul class="nav pull-right" style="margin-top:2%">
               <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user
                      "></span> Akun<b class="caret"></b></a>
                  <ul class="dropdown-menu">
                     <li><a href="{!! URL::to('logout') !!}">Logout</a></li>
                     <li><a href="{!! URL::to('panel') !!}">Panel</a></li>
                  </ul>
               </li>
            </ul>
            @endif
         </ul>
      </div>
      <!--/.nav-collapse -->
   </div>
</nav>