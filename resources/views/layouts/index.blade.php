
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>@yield('title') | Bappeda Kota Banda Aceh</title>

    <!-- Bootstrap core CSS -->
    <link href="{!! URL::to('resources/assets/css/bootstrap.css') !!}" rel="stylesheet">
    <script src="{!! URL::to('resources/assets/jquery-1.9.1.js') !!}"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{!!URL::to('resources/assets/css/footer.css')!!}">

  </head>

  <body>

    <!-- Static navbar -->
    @include('layouts.header')

    @yield('content')
    @include('layouts.footer')
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    @yield('js')
  </body>
</html>
