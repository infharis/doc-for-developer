@extends('layouts.index')
@section('title','Dokumentasi Developer')
@section('content')

<!-- Content -->
<div class="container">
   <div class="row span">
      <div class="col-sm-2">
          <ul id="sidebar" class="nav nav-list bs-docs-sidenav">
            <li><a href="#">The Next Web</a></li>
            <li><a href="#">Mashable</a></li>
            <li><a href="#">TechCrunch</a></li>
            <li><a href="#">GitHub</a></li>
            <li><a href="#">In1</a></li>
            <li><a href="#midCol">TechMeMe</a></li>
        </ul>
      </div>
      <div class="col-sm-3 col-md-10">
        <div id="midCol" data-spy="affix" data-offset-top="300">
        <div class="well"><h3><a href="http://getbootstrap.com">Bootstrap 3 is Here.</a></h3>
               In simple terms, a responsive web design figures out what resolution of
                device it's being served on. Flexible grids then size correctly to fit
                the screen.</div>
        <div class="well">
            <img class="img-responsive" src="//placehold.it/300x300">  
        </div>
        <div class="well">The new Bootstrap 3 is a smaller build. The separate Bootstrap
                base and responsive.css files have now been merged into one. There is no
                more fixed grid, only fluid..</div>
        <div class="well">Well, that's it for now Jim.</div>
        <div class="well">A big design trend for 2013 is "flat" design. Gone are the days of excessive
                gradients and shadows. Designers are producing cleaner flat designs, and
                Bootstrap 3 takes advantage of this minimalist trend.
        Aliquam in felis sit amet augue.</div>
         </div>
      </div>
      
    </div>
</div>

@stop
