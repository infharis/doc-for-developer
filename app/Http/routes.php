<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});


// show form login
Route::get('login','AuthController@ShowForm');
// attempt auth user
Route::post('login','AuthController@login');
Route::get('logout','AuthController@keluar');

//show form register
Route::get('daftar',function(){
	return view('auth.daftar');
});
// post register
Route::post('register','AuthController@PostRegister');
// show page doc
Route::get('doc',function(){
	return view('doc');
});