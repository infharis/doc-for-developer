<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Hash;


/**
*  contoller user auth
*/
class AuthController extends Controller
{
	/**
	 *  show form login
	 */
	public function ShowForm()
	{
		return view('auth.login');
	}
	/**
	 *  attempt auth user
	 */
	public function login(Request $request)
	{
		$this->validate($request,['email'=>'required','password'=>'required']);
		if(Auth::attempt(['email'=>$request->email,'password'=>$request->password],$request->remember))
		{
			return redirect('/');
		}
		return redirect()->back()->with('error','Incorrect Email Dan Password');
	}

	/**
	 *  clear session
	 */
	public function keluar()
	{
		Auth::logout();
		return redirect('/');
	}

	/**
	 *  show form register
	 */
	public function PostRegister(Request $request)
	{
		$this->validate($request,['nama'=>'required','email'=>'required|email|unique:users','password'=>'required|']);
		User::create(['email'=>$request->email,'nama'=>$request->nama,'password'=>Hash::make($request->password)]);
		return redirect('login')->with('success','Success Create Account');
	}
}